package per.wxl.mall.discount.dao;

import per.wxl.mall.discount.entity.HomeSubjectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 专题商品
 * 
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:48:03
 */
@Mapper
public interface HomeSubjectSpuDao extends BaseMapper<HomeSubjectSpuEntity> {
	
}
