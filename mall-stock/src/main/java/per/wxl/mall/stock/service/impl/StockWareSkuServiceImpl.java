package per.wxl.mall.stock.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.Query;

import per.wxl.mall.stock.dao.StockWareSkuDao;
import per.wxl.mall.stock.entity.StockWareSkuEntity;
import per.wxl.mall.stock.service.StockWareSkuService;


@Service("stockWareSkuService")
public class StockWareSkuServiceImpl extends ServiceImpl<StockWareSkuDao, StockWareSkuEntity> implements StockWareSkuService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<StockWareSkuEntity> page = this.page(
                new Query<StockWareSkuEntity>().getPage(params),
                new QueryWrapper<StockWareSkuEntity>()
        );

        return new PageUtils(page);
    }

}