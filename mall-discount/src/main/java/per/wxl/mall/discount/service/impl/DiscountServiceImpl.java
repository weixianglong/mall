package per.wxl.mall.discount.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.Query;

import per.wxl.mall.discount.dao.DiscountDao;
import per.wxl.mall.discount.entity.DiscountEntity;
import per.wxl.mall.discount.service.DiscountService;


@Service("discountService")
public class DiscountServiceImpl extends ServiceImpl<DiscountDao, DiscountEntity> implements DiscountService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DiscountEntity> page = this.page(
                new Query<DiscountEntity>().getPage(params),
                new QueryWrapper<DiscountEntity>()
        );

        return new PageUtils(page);
    }

}