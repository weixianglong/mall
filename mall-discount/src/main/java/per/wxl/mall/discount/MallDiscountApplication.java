package per.wxl.mall.discount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MallDiscountApplication {

	public static void main(String[] args) {
		SpringApplication.run(MallDiscountApplication.class, args);
	}

}
