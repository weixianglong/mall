package per.wxl.mall.discount.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.Query;

import per.wxl.mall.discount.dao.DiscountSpuRelationDao;
import per.wxl.mall.discount.entity.DiscountSpuRelationEntity;
import per.wxl.mall.discount.service.DiscountSpuRelationService;


@Service("discountSpuRelationService")
public class DiscountSpuRelationServiceImpl extends ServiceImpl<DiscountSpuRelationDao, DiscountSpuRelationEntity> implements DiscountSpuRelationService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DiscountSpuRelationEntity> page = this.page(
                new Query<DiscountSpuRelationEntity>().getPage(params),
                new QueryWrapper<DiscountSpuRelationEntity>()
        );

        return new PageUtils(page);
    }

}