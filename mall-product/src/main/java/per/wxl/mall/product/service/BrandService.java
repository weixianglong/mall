package per.wxl.mall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import per.wxl.common.utils.PageUtils;
import per.wxl.mall.product.entity.BrandEntity;

import java.util.Map;

/**
 * 品牌
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:27:02
 */
public interface BrandService extends IService<BrandEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

