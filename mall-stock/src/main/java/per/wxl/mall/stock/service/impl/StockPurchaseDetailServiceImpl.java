package per.wxl.mall.stock.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.Query;

import per.wxl.mall.stock.dao.StockPurchaseDetailDao;
import per.wxl.mall.stock.entity.StockPurchaseDetailEntity;
import per.wxl.mall.stock.service.StockPurchaseDetailService;


@Service("stockPurchaseDetailService")
public class StockPurchaseDetailServiceImpl extends ServiceImpl<StockPurchaseDetailDao, StockPurchaseDetailEntity> implements StockPurchaseDetailService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<StockPurchaseDetailEntity> page = this.page(
                new Query<StockPurchaseDetailEntity>().getPage(params),
                new QueryWrapper<StockPurchaseDetailEntity>()
        );

        return new PageUtils(page);
    }

}