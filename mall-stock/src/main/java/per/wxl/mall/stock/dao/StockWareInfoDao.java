package per.wxl.mall.stock.dao;

import per.wxl.mall.stock.entity.StockWareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:49:24
 */
@Mapper
public interface StockWareInfoDao extends BaseMapper<StockWareInfoEntity> {
	
}
