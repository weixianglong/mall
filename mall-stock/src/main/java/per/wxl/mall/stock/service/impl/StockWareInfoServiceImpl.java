package per.wxl.mall.stock.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.Query;

import per.wxl.mall.stock.dao.StockWareInfoDao;
import per.wxl.mall.stock.entity.StockWareInfoEntity;
import per.wxl.mall.stock.service.StockWareInfoService;


@Service("stockWareInfoService")
public class StockWareInfoServiceImpl extends ServiceImpl<StockWareInfoDao, StockWareInfoEntity> implements StockWareInfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<StockWareInfoEntity> page = this.page(
                new Query<StockWareInfoEntity>().getPage(params),
                new QueryWrapper<StockWareInfoEntity>()
        );

        return new PageUtils(page);
    }

}