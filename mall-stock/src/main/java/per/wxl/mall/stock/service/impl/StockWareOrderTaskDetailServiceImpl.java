package per.wxl.mall.stock.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.Query;

import per.wxl.mall.stock.dao.StockWareOrderTaskDetailDao;
import per.wxl.mall.stock.entity.StockWareOrderTaskDetailEntity;
import per.wxl.mall.stock.service.StockWareOrderTaskDetailService;


@Service("stockWareOrderTaskDetailService")
public class StockWareOrderTaskDetailServiceImpl extends ServiceImpl<StockWareOrderTaskDetailDao, StockWareOrderTaskDetailEntity> implements StockWareOrderTaskDetailService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<StockWareOrderTaskDetailEntity> page = this.page(
                new Query<StockWareOrderTaskDetailEntity>().getPage(params),
                new QueryWrapper<StockWareOrderTaskDetailEntity>()
        );

        return new PageUtils(page);
    }

}