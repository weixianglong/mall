package per.wxl.mall.stock.service;

import com.baomidou.mybatisplus.extension.service.IService;
import per.wxl.common.utils.PageUtils;
import per.wxl.mall.stock.entity.StockPurchaseEntity;

import java.util.Map;

/**
 * 采购信息
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:49:24
 */
public interface StockPurchaseService extends IService<StockPurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

