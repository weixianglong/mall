package per.wxl.mall.stock.dao;

import per.wxl.mall.stock.entity.StockPurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:49:24
 */
@Mapper
public interface StockPurchaseDao extends BaseMapper<StockPurchaseEntity> {
	
}
