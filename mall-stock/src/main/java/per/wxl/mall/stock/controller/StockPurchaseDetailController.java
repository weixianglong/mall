package per.wxl.mall.stock.controller;

import java.util.Arrays;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import per.wxl.mall.stock.entity.StockPurchaseDetailEntity;
import per.wxl.mall.stock.service.StockPurchaseDetailService;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.R;



/**
 * 
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:49:24
 */
@RestController
@RequestMapping("stock/stockpurchasedetail")
public class StockPurchaseDetailController {
    @Autowired
    private StockPurchaseDetailService stockPurchaseDetailService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("stock:stockpurchasedetail:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = stockPurchaseDetailService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("stock:stockpurchasedetail:info")
    public R info(@PathVariable("id") Long id){
		StockPurchaseDetailEntity stockPurchaseDetail = stockPurchaseDetailService.getById(id);

        return R.ok().put("stockPurchaseDetail", stockPurchaseDetail);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("stock:stockpurchasedetail:save")
    public R save(@RequestBody StockPurchaseDetailEntity stockPurchaseDetail){
		stockPurchaseDetailService.save(stockPurchaseDetail);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("stock:stockpurchasedetail:update")
    public R update(@RequestBody StockPurchaseDetailEntity stockPurchaseDetail){
		stockPurchaseDetailService.updateById(stockPurchaseDetail);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("stock:stockpurchasedetail:delete")
    public R delete(@RequestBody Long[] ids){
		stockPurchaseDetailService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
