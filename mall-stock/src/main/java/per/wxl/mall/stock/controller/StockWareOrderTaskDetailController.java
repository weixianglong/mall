package per.wxl.mall.stock.controller;

import java.util.Arrays;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import per.wxl.mall.stock.entity.StockWareOrderTaskDetailEntity;
import per.wxl.mall.stock.service.StockWareOrderTaskDetailService;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.R;



/**
 * 库存工作单
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:49:24
 */
@RestController
@RequestMapping("stock/stockwareordertaskdetail")
public class StockWareOrderTaskDetailController {
    @Autowired
    private StockWareOrderTaskDetailService stockWareOrderTaskDetailService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("stock:stockwareordertaskdetail:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = stockWareOrderTaskDetailService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("stock:stockwareordertaskdetail:info")
    public R info(@PathVariable("id") Long id){
		StockWareOrderTaskDetailEntity stockWareOrderTaskDetail = stockWareOrderTaskDetailService.getById(id);

        return R.ok().put("stockWareOrderTaskDetail", stockWareOrderTaskDetail);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("stock:stockwareordertaskdetail:save")
    public R save(@RequestBody StockWareOrderTaskDetailEntity stockWareOrderTaskDetail){
		stockWareOrderTaskDetailService.save(stockWareOrderTaskDetail);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("stock:stockwareordertaskdetail:update")
    public R update(@RequestBody StockWareOrderTaskDetailEntity stockWareOrderTaskDetail){
		stockWareOrderTaskDetailService.updateById(stockWareOrderTaskDetail);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("stock:stockwareordertaskdetail:delete")
    public R delete(@RequestBody Long[] ids){
		stockWareOrderTaskDetailService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
