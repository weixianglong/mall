package per.wxl.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import per.wxl.common.utils.PageUtils;
import per.wxl.mall.member.entity.MemberCollectSubjectEntity;

import java.util.Map;

/**
 * 会员收藏的专题活动
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:48:54
 */
public interface MemberCollectSubjectService extends IService<MemberCollectSubjectEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

