package per.wxl.mall.stock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MallStockApplication {

	public static void main(String[] args) {
		SpringApplication.run(MallStockApplication.class, args);
	}

}
