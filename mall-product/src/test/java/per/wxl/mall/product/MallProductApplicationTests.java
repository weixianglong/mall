package per.wxl.mall.product;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import per.wxl.mall.product.entity.BrandEntity;
import per.wxl.mall.product.service.BrandService;


@SpringBootTest
class MallProductApplicationTests {
	@Autowired
	public BrandService brandService;
	@Test
	void contextLoads() {
		BrandEntity brandEntity = new BrandEntity();
		brandEntity.setDescript("小米");
		brandEntity.setFirstLetter("H");
		brandService.save(brandEntity);
	}

}
