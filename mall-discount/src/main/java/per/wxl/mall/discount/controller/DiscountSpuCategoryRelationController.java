package per.wxl.mall.discount.controller;

import java.util.Arrays;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import per.wxl.mall.discount.entity.DiscountSpuCategoryRelationEntity;
import per.wxl.mall.discount.service.DiscountSpuCategoryRelationService;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.R;



/**
 * 优惠券分类关联
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:48:03
 */
@RestController
@RequestMapping("discount/discountspucategoryrelation")
public class DiscountSpuCategoryRelationController {
    @Autowired
    private DiscountSpuCategoryRelationService discountSpuCategoryRelationService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("discount:discountspucategoryrelation:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = discountSpuCategoryRelationService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("discount:discountspucategoryrelation:info")
    public R info(@PathVariable("id") Long id){
		DiscountSpuCategoryRelationEntity discountSpuCategoryRelation = discountSpuCategoryRelationService.getById(id);

        return R.ok().put("discountSpuCategoryRelation", discountSpuCategoryRelation);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("discount:discountspucategoryrelation:save")
    public R save(@RequestBody DiscountSpuCategoryRelationEntity discountSpuCategoryRelation){
		discountSpuCategoryRelationService.save(discountSpuCategoryRelation);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("discount:discountspucategoryrelation:update")
    public R update(@RequestBody DiscountSpuCategoryRelationEntity discountSpuCategoryRelation){
		discountSpuCategoryRelationService.updateById(discountSpuCategoryRelation);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("discount:discountspucategoryrelation:delete")
    public R delete(@RequestBody Long[] ids){
		discountSpuCategoryRelationService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
