package per.wxl.mall.stock.service;

import com.baomidou.mybatisplus.extension.service.IService;
import per.wxl.common.utils.PageUtils;
import per.wxl.mall.stock.entity.StockWareInfoEntity;

import java.util.Map;

/**
 * 仓库信息
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:49:24
 */
public interface StockWareInfoService extends IService<StockWareInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

