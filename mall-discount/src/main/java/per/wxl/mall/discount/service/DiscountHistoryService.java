package per.wxl.mall.discount.service;

import com.baomidou.mybatisplus.extension.service.IService;
import per.wxl.common.utils.PageUtils;
import per.wxl.mall.discount.entity.DiscountHistoryEntity;

import java.util.Map;

/**
 * 优惠券领取历史记录
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:48:03
 */
public interface DiscountHistoryService extends IService<DiscountHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

