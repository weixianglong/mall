package per.wxl.mall.discount.controller;

import java.util.Arrays;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import per.wxl.mall.discount.entity.DiscountEntity;
import per.wxl.mall.discount.service.DiscountService;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.R;



/**
 * 优惠券信息
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:48:03
 */
@RestController
@RequestMapping("discount/discount")
public class DiscountController {
    @Autowired
    private DiscountService discountService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("discount:discount:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = discountService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("discount:discount:info")
    public R info(@PathVariable("id") Long id){
		DiscountEntity discount = discountService.getById(id);

        return R.ok().put("discount", discount);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("discount:discount:save")
    public R save(@RequestBody DiscountEntity discount){
		discountService.save(discount);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("discount:discount:update")
    public R update(@RequestBody DiscountEntity discount){
		discountService.updateById(discount);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("discount:discount:delete")
    public R delete(@RequestBody Long[] ids){
		discountService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
