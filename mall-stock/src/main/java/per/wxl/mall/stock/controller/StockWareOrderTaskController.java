package per.wxl.mall.stock.controller;

import java.util.Arrays;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import per.wxl.mall.stock.entity.StockWareOrderTaskEntity;
import per.wxl.mall.stock.service.StockWareOrderTaskService;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.R;



/**
 * 库存工作单
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:49:24
 */
@RestController
@RequestMapping("stock/stockwareordertask")
public class StockWareOrderTaskController {
    @Autowired
    private StockWareOrderTaskService stockWareOrderTaskService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("stock:stockwareordertask:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = stockWareOrderTaskService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("stock:stockwareordertask:info")
    public R info(@PathVariable("id") Long id){
		StockWareOrderTaskEntity stockWareOrderTask = stockWareOrderTaskService.getById(id);

        return R.ok().put("stockWareOrderTask", stockWareOrderTask);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("stock:stockwareordertask:save")
    public R save(@RequestBody StockWareOrderTaskEntity stockWareOrderTask){
		stockWareOrderTaskService.save(stockWareOrderTask);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("stock:stockwareordertask:update")
    public R update(@RequestBody StockWareOrderTaskEntity stockWareOrderTask){
		stockWareOrderTaskService.updateById(stockWareOrderTask);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("stock:stockwareordertask:delete")
    public R delete(@RequestBody Long[] ids){
		stockWareOrderTaskService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
