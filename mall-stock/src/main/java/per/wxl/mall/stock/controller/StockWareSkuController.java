package per.wxl.mall.stock.controller;

import java.util.Arrays;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import per.wxl.mall.stock.entity.StockWareSkuEntity;
import per.wxl.mall.stock.service.StockWareSkuService;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.R;



/**
 * 商品库存
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:49:24
 */
@RestController
@RequestMapping("stock/stockwaresku")
public class StockWareSkuController {
    @Autowired
    private StockWareSkuService stockWareSkuService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("stock:stockwaresku:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = stockWareSkuService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("stock:stockwaresku:info")
    public R info(@PathVariable("id") Long id){
		StockWareSkuEntity stockWareSku = stockWareSkuService.getById(id);

        return R.ok().put("stockWareSku", stockWareSku);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("stock:stockwaresku:save")
    public R save(@RequestBody StockWareSkuEntity stockWareSku){
		stockWareSkuService.save(stockWareSku);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("stock:stockwaresku:update")
    public R update(@RequestBody StockWareSkuEntity stockWareSku){
		stockWareSkuService.updateById(stockWareSku);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("stock:stockwaresku:delete")
    public R delete(@RequestBody Long[] ids){
		stockWareSkuService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
