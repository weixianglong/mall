package per.wxl.mall.discount.controller;

import java.util.Arrays;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import per.wxl.mall.discount.entity.DiscountSpuRelationEntity;
import per.wxl.mall.discount.service.DiscountSpuRelationService;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.R;



/**
 * 优惠券与产品关联
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:48:03
 */
@RestController
@RequestMapping("discount/discountspurelation")
public class DiscountSpuRelationController {
    @Autowired
    private DiscountSpuRelationService discountSpuRelationService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("discount:discountspurelation:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = discountSpuRelationService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("discount:discountspurelation:info")
    public R info(@PathVariable("id") Long id){
		DiscountSpuRelationEntity discountSpuRelation = discountSpuRelationService.getById(id);

        return R.ok().put("discountSpuRelation", discountSpuRelation);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("discount:discountspurelation:save")
    public R save(@RequestBody DiscountSpuRelationEntity discountSpuRelation){
		discountSpuRelationService.save(discountSpuRelation);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("discount:discountspurelation:update")
    public R update(@RequestBody DiscountSpuRelationEntity discountSpuRelation){
		discountSpuRelationService.updateById(discountSpuRelation);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("discount:discountspurelation:delete")
    public R delete(@RequestBody Long[] ids){
		discountSpuRelationService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
