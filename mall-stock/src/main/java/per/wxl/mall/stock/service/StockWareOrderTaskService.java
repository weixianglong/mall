package per.wxl.mall.stock.service;

import com.baomidou.mybatisplus.extension.service.IService;
import per.wxl.common.utils.PageUtils;
import per.wxl.mall.stock.entity.StockWareOrderTaskEntity;

import java.util.Map;

/**
 * 库存工作单
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:49:24
 */
public interface StockWareOrderTaskService extends IService<StockWareOrderTaskEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

