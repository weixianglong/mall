package per.wxl.mall.product.dao;

import per.wxl.mall.product.entity.SpuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息
 * 
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:27:02
 */
@Mapper
public interface SpuInfoDao extends BaseMapper<SpuInfoEntity> {
	
}
