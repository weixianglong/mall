package per.wxl.mall.discount.dao;

import per.wxl.mall.discount.entity.DiscountEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:48:03
 */
@Mapper
public interface DiscountDao extends BaseMapper<DiscountEntity> {
	
}
