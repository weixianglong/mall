package per.wxl.mall.discount.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.Query;

import per.wxl.mall.discount.dao.DiscountSpuCategoryRelationDao;
import per.wxl.mall.discount.entity.DiscountSpuCategoryRelationEntity;
import per.wxl.mall.discount.service.DiscountSpuCategoryRelationService;


@Service("discountSpuCategoryRelationService")
public class DiscountSpuCategoryRelationServiceImpl extends ServiceImpl<DiscountSpuCategoryRelationDao, DiscountSpuCategoryRelationEntity> implements DiscountSpuCategoryRelationService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DiscountSpuCategoryRelationEntity> page = this.page(
                new Query<DiscountSpuCategoryRelationEntity>().getPage(params),
                new QueryWrapper<DiscountSpuCategoryRelationEntity>()
        );

        return new PageUtils(page);
    }

}