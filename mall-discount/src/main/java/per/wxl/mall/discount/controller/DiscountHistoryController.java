package per.wxl.mall.discount.controller;

import java.util.Arrays;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import per.wxl.mall.discount.entity.DiscountHistoryEntity;
import per.wxl.mall.discount.service.DiscountHistoryService;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.R;



/**
 * 优惠券领取历史记录
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:48:03
 */
@RestController
@RequestMapping("discount/discounthistory")
public class DiscountHistoryController {
    @Autowired
    private DiscountHistoryService discountHistoryService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("discount:discounthistory:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = discountHistoryService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("discount:discounthistory:info")
    public R info(@PathVariable("id") Long id){
		DiscountHistoryEntity discountHistory = discountHistoryService.getById(id);

        return R.ok().put("discountHistory", discountHistory);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("discount:discounthistory:save")
    public R save(@RequestBody DiscountHistoryEntity discountHistory){
		discountHistoryService.save(discountHistory);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("discount:discounthistory:update")
    public R update(@RequestBody DiscountHistoryEntity discountHistory){
		discountHistoryService.updateById(discountHistory);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("discount:discounthistory:delete")
    public R delete(@RequestBody Long[] ids){
		discountHistoryService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
