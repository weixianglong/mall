package per.wxl.mall.stock.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.Query;

import per.wxl.mall.stock.dao.StockWareOrderTaskDao;
import per.wxl.mall.stock.entity.StockWareOrderTaskEntity;
import per.wxl.mall.stock.service.StockWareOrderTaskService;


@Service("stockWareOrderTaskService")
public class StockWareOrderTaskServiceImpl extends ServiceImpl<StockWareOrderTaskDao, StockWareOrderTaskEntity> implements StockWareOrderTaskService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<StockWareOrderTaskEntity> page = this.page(
                new Query<StockWareOrderTaskEntity>().getPage(params),
                new QueryWrapper<StockWareOrderTaskEntity>()
        );

        return new PageUtils(page);
    }

}