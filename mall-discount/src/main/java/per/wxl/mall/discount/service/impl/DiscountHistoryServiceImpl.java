package per.wxl.mall.discount.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.Query;

import per.wxl.mall.discount.dao.DiscountHistoryDao;
import per.wxl.mall.discount.entity.DiscountHistoryEntity;
import per.wxl.mall.discount.service.DiscountHistoryService;


@Service("discountHistoryService")
public class DiscountHistoryServiceImpl extends ServiceImpl<DiscountHistoryDao, DiscountHistoryEntity> implements DiscountHistoryService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DiscountHistoryEntity> page = this.page(
                new Query<DiscountHistoryEntity>().getPage(params),
                new QueryWrapper<DiscountHistoryEntity>()
        );

        return new PageUtils(page);
    }

}