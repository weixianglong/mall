package per.wxl.mall.stock.controller;

import java.util.Arrays;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import per.wxl.mall.stock.entity.StockPurchaseEntity;
import per.wxl.mall.stock.service.StockPurchaseService;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.R;



/**
 * 采购信息
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:49:24
 */
@RestController
@RequestMapping("stock/stockpurchase")
public class StockPurchaseController {
    @Autowired
    private StockPurchaseService stockPurchaseService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("stock:stockpurchase:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = stockPurchaseService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("stock:stockpurchase:info")
    public R info(@PathVariable("id") Long id){
		StockPurchaseEntity stockPurchase = stockPurchaseService.getById(id);

        return R.ok().put("stockPurchase", stockPurchase);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("stock:stockpurchase:save")
    public R save(@RequestBody StockPurchaseEntity stockPurchase){
		stockPurchaseService.save(stockPurchase);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("stock:stockpurchase:update")
    public R update(@RequestBody StockPurchaseEntity stockPurchase){
		stockPurchaseService.updateById(stockPurchase);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("stock:stockpurchase:delete")
    public R delete(@RequestBody Long[] ids){
		stockPurchaseService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
