package per.wxl.mall.stock.controller;

import java.util.Arrays;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import per.wxl.mall.stock.entity.StockWareInfoEntity;
import per.wxl.mall.stock.service.StockWareInfoService;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.R;



/**
 * 仓库信息
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:49:24
 */
@RestController
@RequestMapping("stock/stockwareinfo")
public class StockWareInfoController {
    @Autowired
    private StockWareInfoService stockWareInfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("stock:stockwareinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = stockWareInfoService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("stock:stockwareinfo:info")
    public R info(@PathVariable("id") Long id){
		StockWareInfoEntity stockWareInfo = stockWareInfoService.getById(id);

        return R.ok().put("stockWareInfo", stockWareInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("stock:stockwareinfo:save")
    public R save(@RequestBody StockWareInfoEntity stockWareInfo){
		stockWareInfoService.save(stockWareInfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("stock:stockwareinfo:update")
    public R update(@RequestBody StockWareInfoEntity stockWareInfo){
		stockWareInfoService.updateById(stockWareInfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("stock:stockwareinfo:delete")
    public R delete(@RequestBody Long[] ids){
		stockWareInfoService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
