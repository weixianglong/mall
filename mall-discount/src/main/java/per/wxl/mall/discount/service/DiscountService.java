package per.wxl.mall.discount.service;

import com.baomidou.mybatisplus.extension.service.IService;
import per.wxl.common.utils.PageUtils;
import per.wxl.mall.discount.entity.DiscountEntity;

import java.util.Map;

/**
 * 优惠券信息
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:48:03
 */
public interface DiscountService extends IService<DiscountEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

