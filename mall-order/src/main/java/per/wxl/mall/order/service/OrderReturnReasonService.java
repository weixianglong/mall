package per.wxl.mall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import per.wxl.common.utils.PageUtils;
import per.wxl.mall.order.entity.OrderReturnReasonEntity;

import java.util.Map;

/**
 * 退货原因
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:47:12
 */
public interface OrderReturnReasonService extends IService<OrderReturnReasonEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

