package per.wxl.mall.stock.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import per.wxl.common.utils.PageUtils;
import per.wxl.common.utils.Query;

import per.wxl.mall.stock.dao.StockPurchaseDao;
import per.wxl.mall.stock.entity.StockPurchaseEntity;
import per.wxl.mall.stock.service.StockPurchaseService;


@Service("stockPurchaseService")
public class StockPurchaseServiceImpl extends ServiceImpl<StockPurchaseDao, StockPurchaseEntity> implements StockPurchaseService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<StockPurchaseEntity> page = this.page(
                new Query<StockPurchaseEntity>().getPage(params),
                new QueryWrapper<StockPurchaseEntity>()
        );

        return new PageUtils(page);
    }

}