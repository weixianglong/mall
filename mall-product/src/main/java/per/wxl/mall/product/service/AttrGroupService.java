package per.wxl.mall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import per.wxl.common.utils.PageUtils;
import per.wxl.mall.product.entity.AttrGroupEntity;

import java.util.Map;

/**
 * 属性分组
 *
 * @author wxl
 * @email 837790840@qq.com
 * @date 2021-04-04 00:27:02
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

